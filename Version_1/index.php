<?php include_once('includes/header.php');?>
<!-- ORBIT SLIDESHOW -->
<div class="row">
  <div class="columns twelve">
  	<div class="featured orbit" style="height: 240px!important;">
      <div class="orbit-slide row collapse">
        <div class="five columns offset-by-one">
          <h2>This is a slide title</h2>
          <p>The languages only differ in their grammar, their pronunciation and their most common words. </p>
        </div>
        <div class="six columns">
          <img class="right" src="../frameworks/di/400x240/ccc/969696/reland" alt="Overflow: Hidden No More" />
        </div>
      </div>
      <div class="orbit-slide row collapse">
        <div class="five columns offset-by-one">
          <h2>This is another slide title</h2>
          <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary.</p>
        </div>
        <div class="six columns">
          <img class="right" src="../frameworks/di/400x240/ccc/969696/reland" alt="Overflow: Hidden No More" />
        </div>
      </div>
      <div class="orbit-slide row collapse">
        <div class="five columns offset-by-one">
          <h2>Some more titles here</h2>
          <p> If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages</p>
        </div>
        <div class="six columns">
          <img class="right" src="../frameworks/di/400x240/ccc/969696/reland" alt="Overflow: Hidden No More" />
        </div>
      </div>
    </div>
  </div>
</div>

<!-- MAIN CONTENT ROW -->
<div id="main" class="row">
  <!-- LEFT COLUMN -->
  <div class="columns eight"> 
    
    <!-- SERVICES BLOCK -->
    <div class="panel">
    <h2>Services</h2>
        <div class="row">
          <ul id="services" class="block-grid three-up">
          <li>
            <h3><a href="transit.php">Transit and Roads</a></h3>  
            <a href="transit.php"><img src="../frameworks/di/163x92/ccc/969696" alt="demo content image" title="demo content image" align="left"/></a>
          </li>
          <li>
            <h3><a href="transit.php">Municipal Taxes</a></h3>  
            <a href="transit.php"><img src="../frameworks/di/163x92/ccc/969696" alt="demo content image" title="demo content image" align="left"/></a>
          </li>
          <li>
            <h3><a href="transit.php">Municipal Taxes</a></h3>  
            <a href="transit.php"><img src="../frameworks/di/163x92/ccc/969696" alt="demo content image" title="demo content image" align="left"/></a>
          </li>
        </ul>
    		<a href="services.php">More Services...</a>
        </div>
    </div>

    <div class="row">
      <div class="six columns">
        <div class="panel">
        <h2>Press Releases</h2>
    		<ul class="no-bullet">					
    			<li>
    				<h4><a href="news-item.php">IBT Election: Get out to Vote</a></h4>
    				<div>November 21, 2011</div>
    			</li>
    			<li>
            <h4><a href="news-item.php">Why Unions Matter</a></h4> 
            <div>October 21, 2011</div>  
    			</li>
    		</ul>
        <a href="news.php">More News...</a>
        </div>
      </div>
  
      <!-- EVENTS BLOCK -->
    	<div class="six columns">
        <div class="panel">
        <h2>Upcoming Events</h2>
    		<ul class="no-bullet">						
    			<li>
    				<h3><a href="events-item.php">A collection of minds</a></h3>
    				<div>July 30, 2011 | 3:45pm | Vancouver, BC</div>
    			</li>
    			<li>
    				<h3><a href="events-item.php">A collection of minds</a></h3>
    				<div>July 30, 2011 | 3:45pm | Vancouver, BC</div>
    			</li>
    		</ul>
    		<a href="events.php">More events...</a>
    	</div>
      </div>
    </div>

  </div>
  
  <!-- RIGHT COLUMN -->
  <div class="columns four">
    <!-- BLOG BLOCK -->
    <div class="panel">
      <h2>Blog</h2>
      <ul class="no-bullet">
        <li><h3><a href="blog-item.php">Unlimited leaf collection</a></h3><div><em>by John Law on September 20, 2011</em></div>Sed typi typi sed duis eleifend. Ea seacula euismod me sed notare. Processus ea adipiscing qui in...</li>
        <li><h3><a href="blog-item.php">Are you ready for snow?</a></h3><div><em>by Colin Calnan on June 30, 2011</em></div>Sed typi typi sed duis eleifend. Ea seacula euismod me sed notare. Processus ea adipiscing qui in...</li>
        <li><h3><a href="blog-item.php">Keeping your yard trimmings cart clean</a></h3><div><em>by Matt Reimer on January 31, 2011</em></div>Sed typi typi sed duis eleifend. Ea seacula euismod me sed notare. Processus ea adipiscing qui in...</li>
     </ul>
      <a href="blog.php">More Blog Posts...</a>
    </div>
      
  </div>
</div>


<?php include_once('includes/footer.php');?>