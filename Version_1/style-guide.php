<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="blog.php">Blog</a> </li>
    <li>/</li>
    <li class="active">Unlimited leaf collection</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>h1. Style Guide</h1>
    <h3>h3. See how stuff looks</h3>
    <h2>Typography</h2>


          <h4>General Typography</h4>
   
          <div class="typography">

            <h1>h1. This is a very large header.</h1>
            <h2>h2. This is a large header.</h2>
            <h3>h3. This is a medium header.</h3>
            <h4>h4. This is a moderate header.</h4>
            <h5>h5. This is small header.</h5>
            <h6>h6. This is very small header.</h6>

            <p>This is a paragraph. Paragraphs are preset with a font size, line height and spacing to match the overall vertical rhythm. To show what a paragraph looks like this needs a little more content so, let's see...did you know that there are storms occurring on Jupiter that are larger than the Earth? That's pretty cool.</p>

          </div>

          <hr />

          <h4>Header Styles</h4>

          <div class="typography">

            <h2>This is a very large main header.</h2>
            <h4 class="subheader">This is a smaller subheader.</h4>

            <h3>This is a large header. <small>This is a small segment of that header.</small></h3>

          </div>

          <hr />

          <h4>Links</h4>

          <div class="typography">
            <h2><a href="#">This is a header link.</a></h2>
            <h3><a href="#">This is a header link.</a></h3>
            <h4><a href="#">This is a header link.</a></h4>
            <h5><a href="#">This is a header link.</a></h5>
            <p><a href="#">This is a standard inline paragraph link.</a></p>
          </div>

          <hr />

          <h4>Lists</h4>

          <div class="row">

            <div class="four columns">
              <h5>ul.disc</h5>
              <ul class="disc">
                <li>List item with a much longer description or more content.</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
              </ul>
            </div>
            <div class="four columns">
              <h5>ul.circle</h5>
              <ul class="circle">
                <li>List item with a much longer description or more content.</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
              </ul>
            </div>
            <div class="four columns">
              <h5>ul.square</h5>
              <ul class="square">
                <li>List item with a much longer description or more content.</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
                <li>List item</li>
              </ul>
            </div>
          </div>

          <hr />

          <h4>Blockquotes</h4>

          <blockquote>I do not fear computers. I fear the lack of them. <cite>Isaac Asimov</cite></blockquote>

          <hr />





    <hr>
    <h2>h2. Forms</h2>

          <h4>Row Layouts</h4>
          <p>Here's an example of a form layout controlled with rows and columns.</p>

          <form>
            <label>This is a label.</label>
            <input type="text" placeholder="Standard Input" />

            <label>Address</label>
            <input type="text" class="twelve" placeholder="Street" />
            <div class="row">
              <div class="six columns">
                <input type="text" placeholder="City" />
              </div>
              <div class="three columns">
                <input type="text" placeholder="State" />
              </div>
              <div class="three columns">
                <input type="text" placeholder="ZIP" />
              </div>
            </div>
          </form>


          <p>Sometimes you want a form with labels to the left of your inputs. Piece of cake.

          <form>
            <div class="row">
              <div class="two mobile-one columns">
                <label class="right">Address Name:</label>
              </div>
              <div class="ten mobile-three columns">
                <input type="text" placeholder="e.g. Home" class="eight" />
              </div>
            </div>
            <div class="row">
              <div class="two mobile-one columns">
                <label class="right">City:</label>
              </div>
              <div class="ten mobile-three columns">
                <input type="text" class="eight" />
              </div>
            </div>
            <div class="row">
              <div class="two mobile-one columns">
                <label class="right">ZIP:</label>
              </div>
              <div class="ten mobile-three columns">
                <input type="text" class="three" />
              </div>
            </div>
          </form>


          <hr />

          <h4>Fieldsets</h4>
          <p>Simple elements that can contain all or part of a form to create better division.</p>

          <fieldset>

            <legend>Fieldset Name</legend>

            <label>This is a label.</label>
            <input type="text" placeholder="Standard Input" />

            <label>Address</label>
            <input type="text" />
            <input type="text" class="six" />

          </fieldset>


          <hr />

          <h4>Labels and Actions with Collapsed Columns</h4>
 
          <label>Input with a prefix character</label>
          <div class="row">
            <div class="four columns">
              <div class="row collapse">
                <div class="two mobile-one columns">
                  <span class="prefix">#</span>
                </div>
                <div class="ten mobile-three columns">
                  <input type="text" />
                </div>
              </div>
            </div>
          </div>

          <label>Input with a postfix label</label>
          <div class="row">
            <div class="five columns">
              <div class="row collapse">
                <div class="nine mobile-three columns">
                  <input type="text" />
                </div>
                <div class="three mobile-one columns">
                  <span class="postfix">.com</span>
                </div>
              </div>
            </div>
          </div>

          <label>Input with a postfix action (button)</label>
          <div class="row">
            <div class="five columns">
              <div class="row collapse">
                <div class="eight mobile-three columns">
                  <input type="text" />
                </div>
                <div class="four mobile-one columns">
                  <a class="button postfix">Search</a>
                </div>
              </div>
            </div>
          </div>

          <form>
            <fieldset>
              <legend>Large Form Example</legend>

              <div class="row">
                <div class="five columns">

                  <label>Name</label>
                  <input type="text" />

                  <label>Occupation</label>
                  <input type="text" />

                  <label>Twitter</label>
                  <div class="row collapse">
                    <div class="two mobile-one columns">
                      <span class="prefix">@</span>
                    </div>
                    <div class="ten mobile-three columns">
                      <input type="text" placeholder="foundationzurb" />
                    </div>
                  </div>

                  <label>URL</label>
                  <div class="row collapse">
                    <div class="nine mobile-three columns">
                      <input type="text" placeholder="foundation.zurb" />
                    </div>
                    <div class="three mobile-one columns">
                      <span class="postfix">.com</span>
                    </div>
                  </div>

                </div>
              </div>

              <label>Address</label>
              <input type="text" placeholder="Street (e.g. 123 Awesome St.)" />

              <div class="row">
                <div class="six columns">
                  <input type="text" placeholder="City" />
                </div>
                <div class="two columns" />
                  <select>
                    <option>CA</option>
                  </select>
                </div>
                <div class="four columns">
                  <input type="text" placeholder="ZIP" />
                </div>
              </div>

            </fieldset>
          </form>

          <hr />

          <h4>Custom Inputs</h4>
          <form class="custom">
 
            <h5>Radio Buttons and Checkboxes</h5>
            <div class="row">
              <div class="four columns">
                <label for="radio1"><input name="radio1" type="radio" id="radio1" style="display:none;"><span class="custom radio"></span> Radio Button 1</label>
                <label for="radio2"><input name="radio1" type="radio" id="radio2" style="display:none;"><span class="custom radio checked"></span> Radio Button 2</label>
                <label for="radio3"><input name="radio1" type="radio" id="radio3" disabled style="display:none;"><span class="custom radio"></span> Radio Button 3</label>
              </div>
              <div class="four columns">
                <label for="radio4"><input name="radio2" type="radio" id="radio4"> Radio Button 1</label>
                <label for="radio5"><input name="radio2" CHECKED type="radio" id="radio5"> Radio Button 2</label>
                <label for="radio6"><input name="radio2" type="radio" id="radio6"> Radio Button 3</label>
              </div>
              <div class="four columns">
                <label for="checkbox1"><input type="checkbox" id="checkbox1" style="display: none;"><span class="custom checkbox"></span> Label for Checkbox</label>
                <label for="checkbox2"><input type="checkbox" id="checkbox2" checked style="display: none;"><span class="custom checkbox checked"></span> Label for Checkbox</label>
                <label for="checkbox3"><input type="checkbox" CHECKED id="checkbox3"> Label for Checkbox</label>
              </div>
            </div>

            <h5>Dropdown / Select Elements</h5>

            <label for="customDropdown">Dropdown Label</label>
            <select style="display:none;" id="customDropdown">
              <option SELECTED>This is a dropdown</option>
              <option>This is another option</option>
              <option>Look, a third option</option>
            </select>
            <div class="custom dropdown">
              <a href="#" class="current">
                This is a dropdown
              </a>
              <a href="#" class="selector"></a>
              <ul>
                <li>This is a dropdown</li>
                <li>This is another option</li>
                <li>Look, a third option</li>
              </ul>
            </div>

            <label for="customDropdown2">Dropdown Label</label>
            <select id="customDropdown2">
              <option>This is a dropdown</option>
              <option SELECTED>This is another option</option>
              <option>Look, a third option</option>
            </select>


          </form>


<h2>h2. Tables</h2>

  <table>
    <thead>
      <tr>
        <th>Table Header</th>
        <th>Table Header</th>
        <th>Table Header</th>
        <th>Table Header</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Content</td>
        <td>This is longer content</td>
        <td>Content</td>
        <td>Content</td>
      </tr>
      <tr>
        <td>Content</td>
        <td>This is longer content</td>
        <td>Content</td>
        <td>Content</td>
      </tr>
      <tr>
        <td>Content</td>
        <td>This is longer content</td>
        <td>Content</td>
        <td>Content</td>
      </tr>
      <tr>
        <td>Content</td>
        <td>This is longer content</td>
        <td>Content</td>
        <td>Content</td>
      </tr>
    </tbody>
  </table>

<h2>h2. Buttons</h2>

          <h3>Buttons</h3>

          <div class="row">
            <div class="six columns">
              <a href="#" class="tiny button">Tiny Button &raquo;</a><br /><br />
              <a href="#" class="small button">Small Button &raquo;</a><br /><br />
              <a href="#" class="button">Regular Button &raquo;</a><br /><br />
              <a href="#" class="large button">Large Button &raquo;</a><br /><br />
              <br /><br />
              <a href="#" class="tiny secondary button">Secondary Button &raquo;</a><br /><br />
              <a href="#" class="small secondary button">Secondary Button &raquo;</a><br /><br />
              <a href="#" class="secondary button">Secondary Button &raquo;</a><br /><br />
              <a href="#" class="large secondary button">Secondary Button &raquo;</a><br /><br />
              <br /><br />
            </div>
            <div class="six columns">
              <a href="#" class="tiny success button">Success Button &raquo;</a><br /><br />
              <a href="#" class="small success button">Success Button &raquo;</a><br /><br />
              <a href="#" class="success button">Success Button &raquo;</a><br /><br />
              <a href="#" class="large success button">Success Button &raquo;</a><br /><br />
              <br /><br />
              <a href="#" class="tiny alert button">Alert Button &raquo;</a><br /><br />
              <a href="#" class="small alert button">Alert Button &raquo;</a><br /><br />
              <a href="#" class="alert button">Alert Button &raquo;</a><br /><br />
              <a href="#" class="large alert button">Alert Button &raquo;</a><br /><br />
              <br /><br />
            </div>
          </div>


          <button class="button">Form Button</button>
          <input type="submit" class="button" value="Input Submit Button" />
          <a href="#" class="button">Regular Button &raquo;</a>

          <hr />

          <h4>Button Groups</h4>

          <h5>Button Group - Radius</h5>
          <ul class="button-group radius">
            <li><a href="#" class="button radius">Button 1</a></li>
            <li><a href="#" class="button radius">Button 2</a></li>
            <li><a href="#" class="button radius">Button 3</a></li>
          </ul>


          <h5>Even Button Groups</h5>

          <ul class="button-group even three-up">
            <li><a href="#" class="button">Button 1</a></li>
            <li><a href="#" class="button">Button 2</a></li>
            <li><a href="#" class="button">Button 3</a></li>
          </ul>

          <h5>Button Bars</h5>

          <div class="button-bar">
            <ul class="button-group">
              <li><a href="#" class="button">Button 1</a></li>
              <li><a href="#" class="button">Button 2</a></li>
              <li><a href="#" class="button">Button 3</a></li>
            </ul>

            <ul class="button-group">
              <li><a href="#" class="button">Button 1</a></li>
              <li><a href="#" class="button">Button 2</a></li>
              <li><a href="#" class="button">Button 3</a></li>
            </ul>
          </div>

          <p>
            <div href="#" class="large button dropdown">
              Dropdown Button
              <ul>
                <li><a href="#">Dropdown Item</a></li>
                <li><a href="#">Another Dropdown Item</a></li>
                <li class="divider"></li>
                <li><a href="#">Last Item</a></li>
              </ul>
            </div>
          </p>

          <h5>Split Button</h5>

          <p>
            <div href="#" class="large alert button split dropdown">
              <a href="#">Split Button</a>
                <span></span>
              <ul>
                <li><a href="#">Dropdown Item</a></li>
                <li><a href="#">Another Dropdown Item</a></li>
                <li class="divider"></li>
                <li><a href="#">Last Item</a></li>
              </ul>
            </div>
          </p>






          <h4>Glyph Icon Fonts</h4>
            <h5>General</h5>
            <span class="glyph general sample">a b c d e f g h i j k l m n o p q r s t u v w x y z [ ] ; ' , . / 1 2 3 4 5 6 7 8 9 0 - = ! @ # $ % ^ & * ( ) _ +</span>
            <h5>General Enclosed</h5>
            <span class="glyph enclosed sample">a b c d e f g h i j k l m n o p q r s t u v w x y z [ ] ; ' , . / 1 2 3 4 5 6 7 8 9 0 - = ! @ # $ % ^ & * ( ) _ +</span>
            <h5>Social</h5>
            <span class="glyph social sample">a b c d e f g h i j k l m n o p q r s t u v w x y z</span>

          <hr />

        <h3>Fairhead Creative SVG Icons</h3>
        <h4 class="subheader">provided by Zurb</h4>

        <h4>Small Icons</h4>
        
        <a class="fc-webicon behance small" href="#">Behance</a>
        <a class="fc-webicon creativecloud small" href="#">Creative Cloud</a>
        <a class="fc-webicon dribbble small" href="#">Dribbble</a>
        <a class="fc-webicon dropbox small" href="#">Dropbox</a>
        <a class="fc-webicon evernote small" href="#">Evernote</a>
        <a class="fc-webicon facebook small" href="#">Facebook</a>
        <a class="fc-webicon flickr small" href="#">Flickr</a>
        <a class="fc-webicon github small" href="#">Github</a>
        <a class="fc-webicon googleplus small" href="#">GooglePlus</a>
        <a class="fc-webicon icloud small" href="#">iCloud</a>
        <a class="fc-webicon linkedin small" href="#">LinkedIn</a>
        <a class="fc-webicon mail small" href="#">Mail</a>
        <a class="fc-webicon msn small" href="#">MSN</a>
        <a class="fc-webicon pinterest small" href="#">Pinterest</a>
        <a class="fc-webicon rdio small" href="#">Rdio</a>
        <a class="fc-webicon rss small" href="#">RSS</a>
        <a class="fc-webicon skitch small" href="#">Skitch</a>
        <a class="fc-webicon skype small" href="#">Skype</a>
        <a class="fc-webicon spotify small" href="#">Spotify</a>
        <a class="fc-webicon stumbleupon small" href="#">StumbleUpon!</a>
        <a class="fc-webicon twitter small" href="#">Twitter</a>
        <a class="fc-webicon vimeo small" href="#">Vimeo</a>
        <a class="fc-webicon youtube small" href="#">YouTube</a>
        <a class="fc-webicon youversion small" href="#">YouVersion</a>
        <a class="fc-webicon zerply small" href="#">Zerply</a>
        
        <hr>
      
        <h4>Regular Icons</h4>
        <a class="fc-webicon behance" href="#">Behance</a>
        <a class="fc-webicon creativecloud" href="#">Creative Cloud</a>
        <a class="fc-webicon dribbble" href="#">Dribbble</a>
        <a class="fc-webicon dropbox" href="#">Dropbox</a>
        <a class="fc-webicon evernote" href="#">Evernote</a>
        <a class="fc-webicon facebook" href="#">Facebook</a>
        <a class="fc-webicon flickr" href="#">Flickr</a>
        <a class="fc-webicon github" href="#">Github</a>
        <a class="fc-webicon googleplus" href="#">GooglePlus</a>
        <a class="fc-webicon icloud" href="#">iCloud</a>
        <a class="fc-webicon linkedin" href="#">LinkedIn</a>
        <a class="fc-webicon mail" href="#">Mail</a>
        <a class="fc-webicon msn" href="#">MSN</a>
        <a class="fc-webicon pinterest" href="#">Pinterest</a>
        <a class="fc-webicon rdio" href="#">Rdio</a>
        <a class="fc-webicon rss" href="#">RSS</a>
        <a class="fc-webicon skitch" href="#">Skitch</a>
        <a class="fc-webicon skype" href="#">Skype</a>
        <a class="fc-webicon spotify" href="#">Spotify</a>
        <a class="fc-webicon stumbleupon" href="#">StumbleUpon!</a>
        <a class="fc-webicon twitter" href="#">Twitter</a>
        <a class="fc-webicon vimeo" href="#">Vimeo</a>
        <a class="fc-webicon youtube" href="#">YouTube</a>
        <a class="fc-webicon youversion" href="#">YouVersion</a>
        <a class="fc-webicon zerply" href="#">Zerply</a>
        
        <hr>
      
        <h4>Large Icons</h4>
        <a class="fc-webicon behance large" href="#">Behance</a>
        <a class="fc-webicon creativecloud large" href="#">Creative Cloud</a>
        <a class="fc-webicon dribbble large" href="#">Dribbble</a>
        <a class="fc-webicon dropbox large" href="#">Dropbox</a>
        <a class="fc-webicon evernote large" href="#">Evernote</a>
        <a class="fc-webicon facebook large" href="#">Facebook</a>
        <a class="fc-webicon flickr large" href="#">Flickr</a>
        <a class="fc-webicon github large" href="#">Github</a>
        <a class="fc-webicon googleplus large" href="#">GooglePlus</a>
        <a class="fc-webicon icloud large" href="#">iCloud</a>
        <a class="fc-webicon linkedin large" href="#">LinkedIn</a>
        <a class="fc-webicon mail large" href="#">Mail</a>
        <a class="fc-webicon msn large" href="#">MSN</a>
        <a class="fc-webicon pinterest large" href="#">Pinterest</a>
        <a class="fc-webicon rdio large" href="#">Rdio</a>
        <a class="fc-webicon rss large" href="#">RSS</a>
        <a class="fc-webicon skitch large" href="#">Skitch</a>
        <a class="fc-webicon skype large" href="#">Skype</a>
        <a class="fc-webicon spotify large" href="#">Spotify</a>
        <a class="fc-webicon stumbleupon large" href="#">StumbleUpon!</a>
        <a class="fc-webicon twitter large" href="#">Twitter</a>
        <a class="fc-webicon vimeo large" href="#">Vimeo</a>
        <a class="fc-webicon youtube large" href="#">YouTube</a>
        <a class="fc-webicon youversion large" href="#">YouVersion</a>
        <a class="fc-webicon zerply large" href="#">Zerply</a>



    <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>

    <div class="row">
      <h3>Comments</h3>
      <div class="comments well">
        <div class="eight columns"><strong>1.</strong> Neil</div> <div class="four columns">July 28th 2009 @ 08:39PM</div>
        <p>How do you feel about this method?  I'm not criticizing, I'm just curious on the difference.</p>
        <p>Keep the HTML the same</p>
      </div>
    </div>

    <div class="column">
      <h3>Add new comment</h3>
      <form class="" action="">
        <div class="clearfix">
          <label for="edit-author--2">Your name </label>
          <div class="input">
            <input type="text" id="edit-author--2" name="subject" value="" class="oversize">
          </div>
        </div>
        <div class="clearfix">
          <label for="edit-subject">Subject </label>
          <div class="input">
            <input type="text" id="edit-subject" name="subject" value="" class="oversize">
          </div>
        </div>
        <div class="clearfix">
          <label for="comment-body">Comment <span class="form-required" title="This field is required.">*</span></label>
          <div class="input clearfix" >
            <textarea class="xlarge" id="comment-body" name="comment-body" cols="60" rows="5"></textarea>
          </div>
        </div>
        <div>
          <input type="submit" id="edit-submit" name="op" value="Save" class="nice button">
          <input type="submit" id="edit-preview" name="op" value="Preview" class="nice button">
        </div>

      </form>

    </div> 
 
  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Blog Categories</h2>
      <ul>
        <li><a href="">Waste Diposal</a></li>
        <li><a href="">Snow clearing</a></li>
        <li><a href="">Garden Trimmings</a></li>
        <li><a href="">Property Taxes</a></li>
      </ul>
    </div>
  </div>
  
  <div id="content" class="columns four"> 
    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>