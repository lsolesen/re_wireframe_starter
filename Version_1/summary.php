<?php require_once( '../frameworks/PHP/directorylist.php' ); ?>


<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />
	
	<title><?php print get_name(); ?> Wireframes</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/foundation.css">
	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/app.css">
  <link rel="stylesheet" href="../frameworks/foundation/icons/icon-fonts.css">
  <link rel="stylesheet" href="../frameworks/foundation/icons/fc-webicons.css">
  <link rel="stylesheet" href="../frameworks/superfish.css">

	<link rel="stylesheet" href="style.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="foundation/stylesheets/ie.css">
	<![endif]-->

	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body>


<div class="row">
  <div class="header panel callout">
  <h1>Directory of Templates: <?php print get_name(); ?></h1>
  <h4><a href="../">(Back)</a></h4>
  </div>
</div>

<div class="row">
<ul class="block-grid three-up">
<?php print template_directory( getcwd() ); ?>
</ul>
</div>


	<!-- Included JS Files -->
	<!-- Combine and Compress These JS Files -->
  <script src="../frameworks/javascripts/jquery.min.js"></script>
	<script src="../frameworks/foundation/javascripts/app.js"></script>
	<script src="../frameworks/foundation/javascripts/foundation.js"></script>

	<!-- End Combine and Compress These JS Files -->
	<script src="script.js"></script>
	

</body>
</html>