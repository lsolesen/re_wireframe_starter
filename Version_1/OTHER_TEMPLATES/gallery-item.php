<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="media.php">Media</a> </li>
    <li>/</li>
    <li><a href="galleries.php">Photo Galleries</a> </li>
    <li>/</li>
    <li class="active">10K Charity Run</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  

  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>10K Charity Run</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec mauris pulvinar erat faucibus euismod. Donec rutrum euismod libero, vel hendrerit arcu rhoncus sit amet. Fusce vel augue ac libero luctus ornare. Phasellus accumsan dapibus tincidunt.</p>
    <!-- ORBIT SLIDESHOW -->
  	<div class="gallery orbit">
       <img src="../frameworks/di/582x4:3/ccc/969696/reland" alt="Overflow: Hidden No More" />
       <img src="../frameworks/di/582x4:3/ccc/969696/reland" alt="Overflow: Hidden No More" />
       <img src="../frameworks/di/582x4:3/ccc/969696/reland" alt="Overflow: Hidden No More" />
    </div>
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h3>Media Room</h3>
      <ul>
        <li><a href="news.php">News</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">Videos</a></li>
     </ul>
    </div>

    <div class="panel">
      <h4>Popular Content</h4>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>