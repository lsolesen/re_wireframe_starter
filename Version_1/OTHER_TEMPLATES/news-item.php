<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="media.php">Media Room</a> </li>
    <li>/</li>
    <li><a href="news.php">Press Releases</a> </li>
    <li>/</li>
    <li class="active">Why Unions Matter</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 
    <h1>Why Unions Matter</h1>
    <div class="subheader">October 21, 2011</div>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
    <p>Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus. Mauris id blandit orci. Vestibulum facilisis, dui id placerat egestas, erat erat gravida neque, nec blandit massa mauris nec erat. Etiam eu ligula libero. Aenean dictum malesuada felis. Aenean orci erat, interdum a dictum nec, pellentesque eget justo. Aliquam erat volutpat. Nam consequat ultrices massa, sit amet sodales massa imperdiet quis. Mauris placerat arcu vel sapien vestibulum et molestie diam iaculis. Sed malesuada fringilla augue nec porttitor. Sed accumsan magna ipsum, id malesuada lectus.</p>
     <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h4>Media Room</h4>
      <ul>
        <li><a href="news.php">Press Releases</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">videos</a></li>
      </ul>
    </div>

    <div class="panel">
      <h4>Archive by date</h4>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>