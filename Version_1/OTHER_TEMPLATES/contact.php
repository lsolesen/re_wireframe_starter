<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li class="active">Contact</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  

  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Contact</h1>
      
      <form class="form-stacked">
      <div class="clearfix">
        <label for="edit-name">Your name <span class="form-required" title="This field is required.">*</span></label>
        <input type="text" class="xxlarge">
      </div>
      <div class="clearfix">
        <label for="edit-mail">Your e-mail address <span class="form-required" title="This field is required.">*</span></label>
        <input type="text" class="xxlarge">
      </div>
      <div class="clearfix">
        <label for="edit-subject">Subject <span class="form-required" title="This field is required.">*</span></label>
        <input type="text" class="xxlarge">
      </div>
      <div class="clearfix">
        <label for="edit-message">Message <span class="form-required" title="This field is required.">*</span></label>
       <div>
          <textarea id="edit-message" name="message" cols="60" rows="5" class="xxlarge"></textarea>
      </div>
      </div>
      <div class="clearfix">
        <input type="checkbox" id="edit-copy" name="copy" value="1">  
        <label class="option" for="edit-copy">Send yourself a copy. </label>
      </div>
      <div class="captcha">
        <img src="images/captcha.jpg" width="180" height="60" alt="Image CAPTCHA" title="Image CAPTCHA">
        <label for="edit-captcha-response">What code is in the image? <span class="form-required" title="This field is required.">*</span></label>
        <input type="text" id="edit-captcha-response" name="captcha_response" value="" size="15" maxlength="128" class="form-text required" autocomplete="off">
        <div class="description">Enter the characters shown in the image.</div>
      </div>
      <p>&nbsp;</p>
      <div>
        <input type="submit" value="Send message" class="nice small blue radius button">
      </div>
    </form>
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h4>Popular Content</h4>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>