<?php include_once('includes/header.php');?>

<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li class="active">Media Room</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Media Room</h1>
    <p>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! "Now fax quiz Jack! " my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. </p>
    
    <div class="row">
      <div class="columns">
        <h2>Press Releases</h2>  
        <div>
          <h4><a href="news-item.php">IBT Election: Hoffa-Hall Slate Receives 60 Percent Of Vote</a></h4>
          <div>November 21, 2011</div>
          <p>General President Jim Hoffa, his running mate for General Secretary-Treasurer Ken Hall and their entire slate, were elected by a wide margin in the 2011 Election of International Union Officers.</p>
        </div>
        <div>
          <h4><a href="news-item.php">Why Unions Matter</a></h4> 
          <div>October 21, 2011</div>  
          <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.</p>
        </div>
        <div>
          <h4><a href="news-item.php">More interesting Stuff</a></h4> 
          <div>September 1, 2011</div>  
          <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
        </div>
        <div>
          <h4><a href="news-item.php">Some other long title</a></h4> 
          <div>June 11, 2011</div>  
          <p>I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty.</p>
        </div>
        <div>
          <h4><a href="news-item.php">And yet another thing that is making the news today</a></h4> 
          <div>April 21, 2011</div>  
          <p>General President Jim Hoffa, his running mate for General Secretary-Treasurer Ken Hall and their entire slate, were elected by a wide margin in the 2011 Election of International Union Officers.</p>
        </div>
    		<a href="news.php">More Press Releases...</a>
      </div>
    </div>
    <div class="row">
      <div class="six columns">
        <h2>Galleries</h2>
  			<div class="">
          <div class="">
            <a href="gallery-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/reland">
    				<h4><a href="gallery-item">A Big day out</a></h4>
    			</div>
    			<div  class="">
            <a href="gallery-item.php"><img src="../frameworks/di/16:9x99/ccc/9696969/reland">
    				<h4><a href="gallery-item.php">10K Charity Run</a></h4>
    			</div>
        </div>
    		<a href="galleries.php">More photo galleries...</a>
      </div>
      <div class="six columns">
        <h2>Videos</h2>
        <div class="row">
    			<div class="">
            <a href="video-item.php"><img src="../frameworks/di/16:9x99/video">
    				<h4><a href="video-item.php">First council meeting</a></h4>
    			</div>
    			<div class="">
            <a href="video-item.php"><img src="../frameworks/di/16:9x99/video">
    				<h4><a href="video-item.php">Interview with Mayor</a></h4>
    			</div>
        </div>
    		<a href="videos.php">More videos...</a>
      </div>
    </div>
  
 </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h3>Media Room</h3>
      <ul>
        <li><a href="news.php">Press Releases</a></li>
        <li><a href="galleries.php">Photo Galleries</a></li>
        <li><a href="videos.php">Videos</a></li>
     </ul>
    </div>

    <div class="panel">
      <h3>Popular Content</h3>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>

    <div class="panel">
      <h2>Press Contact</h2>
  			<div class="row">			
          <div class="three columns">
            <img src="../frameworks/di/125x3:4/ccc/969696/bust">
          </div>
          <div class="nine columns ">
            <h3><a href="emira">Sherri Maunsell</a></h3>
            <div class="subheader">Director of the Press</div>
  				  <div>604.555.5555<br /><a href="">sherri@company.com</a></div>
  				</div>
        </div>
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>