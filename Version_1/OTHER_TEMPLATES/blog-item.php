<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a> </li>
    <li>/</li>
    <li><a href="blog.php">Blog</a> </li>
    <li>/</li>
    <li class="active">Unlimited leaf collection</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Unlimited leaf collection</h1>
    <div class="subheader"><a href="">Colin Calnan</a><br />November 21, 2011 <span class="divider">|</span> <a href="">2 comments</a></div>  
    <p>It is now fall and the City will be collecting unlimited quantities of leaves from your home (every second week) from October 1 to January 31. 
    <p>Fill your yard trimmings cart (green lid) with your leaves and set it out on your scheduled yard trimmings collection day. Remember, no plastic bags or liners. Use a standard store-bought garbage can or a biodegradable paper bag for your extra leaves. Please do not rake or blow leaves onto the street.</p>
    <p>You can also compost or mulch your leaves as an alternative to setting them out to be collected.</p>
    <p>Please help clear leaves from around drains (catch basins) to avoid flooding.</p>
    <p><div class="field-label">Filed In:</div>
      <div class="field-item even"><a href="/blog-terms/snow-clearing">Snow clearing</a></div>
			<div class="field-item odd"><a href="/blog-terms/drupal">Drupal</a></div>
    </p>

    <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>

    <div class="row">
      <div class="columns twelve">

        <h3>Comments</h3>
        <div class="comments well">
          <div class="eight columns"><strong>1.</strong> Neil</div> <div class="four columns">July 28th 2009 @ 08:39PM</div>
          <p>How do you feel about this method?  I'm not criticizing, I'm just curious on the difference.</p>
          <p>Keep the HTML the same</p>
        </div>

        <h3>Add new comment</h3>
        <form class="" action="">
          <div class="clearfix">
            <label for="edit-author--2">Your name </label>
            <div class="input">
              <input type="text" id="edit-author--2" name="subject" value="" class="oversize">
            </div>
          </div>
          <div class="clearfix">
            <label for="edit-subject">Subject </label>
            <div class="input">
              <input type="text" id="edit-subject" name="subject" value="" class="oversize">
            </div>
          </div>
          <div class="clearfix">
            <label for="comment-body">Comment <span class="form-required" title="This field is required.">*</span></label>
            <div class="input clearfix" >
              <textarea class="xlarge" id="comment-body" name="comment-body" cols="60" rows="5"></textarea>
            </div>
          </div>
          <div>
            <input type="submit" id="edit-submit" name="op" value="Save" class="nice button">
            <input type="submit" id="edit-preview" name="op" value="Preview" class="nice button">
          </div>
        </form>
      </div>
    </div> 
 
  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Blog Categories</h2>
      <ul>
        <li><a href="">Waste Diposal</a></li>
        <li><a href="">Snow clearing</a></li>
        <li><a href="">Garden Trimmings</a></li>
        <li><a href="">Property Taxes</a></li>
      </ul>
    </div>
  </div>
  
  <div id="content" class="columns four"> 
    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>