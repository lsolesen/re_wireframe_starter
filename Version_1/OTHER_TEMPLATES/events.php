<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
  <ul class="link-list">
    <li><a href="index.php">Home</a></li>
    <li>/</li>
    <li class="active">Events</li>
  </ul>
  </div>
</div>

<div id="main" class="row">  
  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>Events</h1>
      <div>
        <h2><a href="events-item.php">A collection of minds</a></h2> 
        <div>November 2, 2011 - November 3, 2011</div>  
        <div>1:00 pm - 1:30 pm</div>  
        <div>Viscount Gort Hotel Winnipeg, MB</div><br />
        <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel.</p>
        <a href="">Read more...</a>  
      </div>
      <div>
        <h2><a href="events-item.php">A Celebration of Achievements</a></h2> 
        <div>September 16, 2011 - September 18, 2011</div>  
        <div>6:00 pm - 11:30 pm</div>  
        <p>Delta Ottawa Hotel Ottawa, ON</p><br />
        <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel.</p>
        <a href="">Read more...</a>  
      </div>
      <div>
        <h2><a href="events-item.php">Transformation and Innovation</a></h2> 
        <div>September 7, 2011</div>  
        <div>8:00 am - 5:30 pm</div>
        <div>Westin Bayshore, Vancouver, BC</div><br />
        <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel.</p>
        <a href="">Read more...</a>  
      </div>
      <div>
        <h2><a href="events-item.php">A Celebration of Achievements</a></h2> 
        <div>June 16, 2011</div>  
        <div>6:00 pm - 11:30 pm</div>  
        <div>Fairmont Hotel, Vancouver, BC</div><br />
        <p>I should be incapable of drawing a single stroke at the present moment; and yet I feel.</p>
        <a href="">Read more...</a>  
      </div>
    <!-- PAGINATION -->
    <ul class="pagination">
      <li class="unavailable"><a href="">&laquo;</a></li>
      <li class="current"><a href="">1</a></li>
      <li><a href="">2</a></li>
      <li><a href="">3</a></li>
      <li><a href="">4</a></li>
      <li class="unavailable"><a href="">&hellip;</a></li>
      <li><a href="">12</a></li>
      <li><a href="">13</a></li>
      <li><a href="">&raquo;</a></li>
    </ul>

  </div>
  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h2>Events</h2>
      <ul>
        <li><a href="events-calendar.php">Full Calendar</a></li>
        <li><a href="events-past.php">Past Events</a></li>
      </ul>
    </div>

    <div class="panel">
      <h2>Archive by date</h2>
      <ul class="views-summary">
        <li><a href="/news-media/news-releases/archive/2011" class="active">2011 (22)</a></li>
        <li><a href="/news-media/news-releases/archive/2010">2010 (23)</a></li>
        <li><a href="/news-media/news-releases/archive/2009">2009 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2008">2008 (10)</a></li>
        <li><a href="/news-media/news-releases/archive/2007">2007 (24)</a></li>
        <li><a href="/news-media/news-releases/archive/2006">2006 (12)</a></li>
        <li><a href="/news-media/news-releases/archive/2005">2005 (32)</a></li>
        <li><a href="/news-media/news-releases/archive/2004">2004 (44)</a></li>
        <li><a href="/news-media/news-releases/archive/2003">2003 (21)</a></li>
        <li><a href="/news-media/news-releases/archive/2002">2002 (20)</a></li>
        <li><a href="/news-media/news-releases/archive/2001">2001 (11)</a></li>
        <li><a href="/news-media/news-releases/archive/2000">2000 (6)</a></li>
      </ul>    
    </div>
  </div>

</div><!-- ROW-->


<?php include_once('includes/footer.php');?>