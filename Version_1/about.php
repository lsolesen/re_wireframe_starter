<?php include_once('includes/header.php');?>
<!-- BREADCRUMB -->
<div class="row">
  <div class="columns twelve">
    <ul class="link-list">
      <li><a href="index.php">Home</a></li>
      <li>/</li>
      <li class="active">About</li>
    </ul>
  </div>
</div>

<div id="main" class="row">  


  <!-- MAIN CONTENT-->
  <div id="content" class="columns eight"> 

    <h1>About Us</h1>
    <p><img src="../frameworks/di/255x220/ccc/969696" class="left" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec mauris pulvinar erat faucibus euismod. Donec rutrum euismod libero, vel hendrerit arcu rhoncus sit amet. Fusce vel augue ac libero luctus ornare. Phasellus accumsan dapibus tincidunt. Vestibulum mattis suscipit diam vitae egestas. Etiam sed ipsum id eros scelerisque molestie ut eget neque. Donec accumsan tincidunt imperdiet. Nullam varius, mi eget tincidunt dignissim, dolor dui mattis mi, vitae aliquam mi nulla vitae nisl. Pellentesque luctus turpis a nisi feugiat sed molestie purus bibendum. Duis purus arcu, bibendum pharetra aliquet quis, vestibulum quis magna. Aliquam luctus posuere pellentesque.</p>
    <p>Ut quis lorem elit. Praesent ut est nisi. Sed egestas nisl a felis imperdiet ac scelerisque sem consequat. Suspendisse sagittis, ligula id euismod eleifend, lectus dolor dignissim libero, eu dapibus eros diam eu quam. Maecenas justo urna, mollis at auctor quis, posuere id mauris. Mauris porttitor ligula id tortor rutrum sed ultricies libero congue. Duis porta quam nec eros euismod at sagittis risus vulputate. Duis a nunc erat, ut viverra lectus. Duis mi nulla, pulvinar ut venenatis facilisis, viverra vitae nulla. Ut et neque at elit laoreet vehicula vitae quis nisl. Suspendisse potenti. In a erat et ligula euismod porta.</p>
    <p>Ut ut felis dui, ut commodo tortor. Etiam dapibus condimentum enim, nec consequat orci porttitor ac. Sed semper gravida mi in malesuada. Nullam scelerisque gravida felis a vestibulum. Nulla facilisi. Aliquam quis commodo mi. Duis vestibulum, ligula id venenatis semper, quam sapien fermentum ipsum, a cursus elit lorem ac tortor.</p>
    <p>Aliquam id ultricies metus. Maecenas pretium magna porta tortor euismod volutpat. Nunc turpis elit, commodo in commodo vitae, porttitor ac urna. Vestibulum sollicitudin risus metus. In justo magna, facilisis nec mattis fermentum, scelerisque eu urna. Suspendisse a tellus eros, aliquam pretium tortor. Curabitur velit tellus, molestie ac ultricies ut, gravida nec nisi. Vivamus faucibus ipsum vel justo pulvinar sollicitudin. Integer non elit velit, in placerat odio. Sed bibendum sapien accumsan risus feugiat sit amet lobortis mi dapibus. Integer sed eros vel diam elementum volutpat eu et justo. Aliquam semper tristique neque, ac facilisis tortor placerat at. Maecenas urna augue, varius quis facilisis vel, fringilla adipiscing lectus. Integer bibendum eleifend turpis, id accumsan nisl sodales vel. Nam felis augue, condimentum et accumsan ac, fringilla eget risus.</p>
    <p>
    <table>
			<thead>
				<tr>
					<th>Table Header</th>
					<th>Table Header</th>
					<th>Table Header</th>
					<th>Table Header</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Content</td>
					<td>This is longer content</td>
					<td>Content</td>
					<td>Content</td>
				</tr>
				<tr>
					<td>Content</td>
					<td>This is longer content</td>
					<td>Content</td>
					<td>Content</td>
				</tr>
				<tr>
					<td>Content</td>
					<td>This is longer content</td>
					<td>Content</td>
					<td>Content</td>
				</tr>
				<tr>
					<td>Content</td>
					<td>This is longer content</td>
					<td>Content</td>
					<td>Content</td>
				</tr>
			</tbody>
		</table>
    <p>Vivamus pharetra elementum varius. Maecenas a rhoncus ligula. Suspendisse ut dolor in nunc vestibulum ultrices ut at turpis. Aliquam commodo leo at odio congue vulputate. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc mollis scelerisque leo sed rhoncus. Aliquam ut justo felis, pharetra scelerisque dolor. Quisque vel est sit amet ipsum imperdiet congue. Ut egestas porta nisl sed commodo. Morbi hendrerit quam sodales nibh aliquet vel sagittis lorem ultrices. Quisque posuere, sapien pretium aliquam ornare, felis arcu imperdiet mi, in molestie magna lacus at enim. Fusce quis lorem neque, id pretium tortor. Nullam ullamcorper felis augue. Nunc nec erat libero, vitae semper lacus. Duis euismod blandit nibh vitae convallis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
  
     <div class="row">
      <div class="eight columns offset-by-four">
      <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
          <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
          <a class="addthis_button_tweet"></a>
          <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
          <a class="addthis_counter addthis_pill_style"></a>
        </div>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4ee13bd71905212d"></script>
        <!-- AddThis Button END -->
      </div>
    </div>
 
  </div>

  <!-- SIDEBAR CONTENT-->
  <div id="sidebar" class="columns four"> 
    <div class="panel">
      <h4>About</h4>
      <ul>
        <li><a href="#nice1">Sub Page 1</a>
          <ul>
            <li><a href="#nice1">Sub Sub Page 1</a></li>
          </ul>
        </li>
        <li><a href="#nice1">Sub Page 2</a></li>
        <li><a href="#nice1">Sub Page 3</a></li>
      </ul>
    </div>

    <div class="panel">
      <h4>Popular Content</h4>
      <ul>
        <li><a href="#nice1">Link to popular content A</a></li>
        <li><a href="#nice1">Some other service maybe</a></li>
        <li><a href="#nice1">A nice blog post</a></li>
        <li><a href="#nice1">A link to the Director's profile</a></li>
        <li><a href="#nice1">Take out the trash</a></li>
      </ul>
    </div>
  </div>


</div><!-- ROW-->


<?php include_once('includes/footer.php');?>