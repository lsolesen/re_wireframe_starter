<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	
	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />
	
	<title>Wireframes</title>
  
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/foundation.css">

	<link rel="stylesheet" href="../frameworks/foundation/stylesheets/app.css">
  <link rel="stylesheet" href="../frameworks/foundation/icons/icon-fonts.css">
  <link rel="stylesheet" href="../frameworks/foundation/icons/fc-webicons.css">

	<link rel="stylesheet" href="style.css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="../../framework/foundation/stylesheets/ie.css">
	<![endif]-->

	
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>
<body class="svg">
  <div id="mobile-nav-trigger" class="show-for-small row">
    <div class="columns phone-two">
      <a id="menu-link" data-reveal-id="mobile-nav" href="#">Mobile Menu</a>
    </div>
  </div>




  <div class="reveal-modal" id="mobile-nav">
    <a class="close-reveal-modal">&#215;</a>
    <ul class="no-bullet">
      <li><a href="index.php" class="large button">Home</a></li>
      <li><a href="about.php" class="large button">About</a></li>
      <li><a href="services.php" class="large button">Services</a></li>
      <li><a href="resources.php" class="large button">Resources</a></li>
      <li><a href="media.php" class="large button">Media Room</a></li>
      <li><a href="blog.php" class="large button">Blog</a></li>
      <li><a href="events.php" class="large button">Events</a></li>
      <li><a href="contact.php" class="large button">Contact</a></li>
    </ul>
  </div>

  	<header class="row">
      <div class="nine columns">
        <a href="index.php"><img class="logo" src="../frameworks/di/200x110/ccc/969696&text=Your+Logo" alt="logo" /></a>
      </div>
      <div class="three columns">
        <form method="get" class="nice" id="searchform" action="">
        <div class="row">&nbsp;</div>
        <div class="row collapse">
          <div class="eight mobile-three columns">
            <input type="text" class="" name="s" id="s" placeholder="Search">
          </div>
          <div class="four mobile-one columns">
            <input type="submit" class="submit postfix button " name="submit" id="searchsubmit" value="Go">
          </div>
        </div>

        </form>
        <div class="row">
          <div class="columns four change-site-link"></div>
          <div class="columns four"><a href="">English</a></div>
          <div class="columns four"><a href="">Fran&ccedil;ais</a></div>
        </div>
      </div>
  	</header>
  
    <nav class="row hide-for-small">
      <div class="columns twelve">
      <ul class="nav-bar">
        <li><a href="index.php" class="">Home</a></li>
        <li class="has-flyout"><a href="about.php" class="">About</a>
          <a href="#" class="flyout-toggle"></a>
          <ul class="flyout">
        		<li>
        			<a href="leadership.php">Leadership</a>
        		</li>
        		<li>
        			<a href="staff.php">Staff Directory</a>
        		</li>
          </ul>
        </li>
        <li class="has-flyout">
          <a href="services.php" class="">Services</a>
          <a href="#" class="flyout-toggle"></a>
          <ul class="flyout">
        		<li>
        			<a href="transit.php">Transit and Roads</a>
        		</li>
        		<li class="has-flyout">
        			<a href="#ab">Municipal Taxes</a>
              <a href="#" class="flyout-toggle"></a>
        			<ul class="flyout">
        				<li><a href="#">Property Taxes</a></li>
        				<li><a href="#aba">Corporation Taxes</a></li>
        				<li><a href="#abb">Vehicle Taxes</a></li>
        			</ul>
        		</li>
        		<li>
        			<a href="#aa">Garbage Collection</a>
        		</li>
        		<li>
        			<a href="#aa">Parks and Recreation</a>
        		</li>
    
        	</ul>  
        </li>
        <li class="has-flyout"><a href="resources.php" class="">Resources</a>
          <a href="#" class="flyout-toggle"></a>
          <ul class="flyout">
            <li>
              <a href="datasets.php">Datasets</a>
            </li>
            <li>
              <a href="documents.php">Documents</a>
            </li>
          </ul>
        </li>
        <li class="has-flyout"><a href="media.php" class="">Media Room</a>
          <a href="#" class="flyout-toggle"></a>
          <ul class="flyout">
            <li><a href="news.php">Press Releases</a></li>
            <li><a href="galleries.php">Photo Galleries</a></li>
            <li><a href="videos.php">Videos</a></li>
          </ul>
        </li>
        <li><a href="blog.php" class="">Blog</a></li>
        <li class="has-flyout"><a href="events.php" class="">Events</a>
          <a href="#" class="flyout-toggle"></a>
          <ul class="flyout">
            <li>
              <a href="events-calendar.php">Full Calendar</a>
            </li>
            <li>
              <a href="events-past.php">Past Events</a>
            </li>
          </ul>
        </li>
        <li><a href="contact.php" class="">Contact</a></li>
      </ul>
      </div>
    </nav>
